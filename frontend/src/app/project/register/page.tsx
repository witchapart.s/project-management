"use client"
import { useEffect, useState } from "react"

// master data
const mdPolicy_list = [
    { id: "MD-001", name: "Sales" },
    { id: "MD-002", name: "Delivery" },
    { id: "MD-003", name: "Cost" }
]
const costSavingTypeList = [
    { id: "cst-001", name: "Productivity" },
    { id: "cst-002", name: "Cost" },
    { id: "cst-003", name: "Delivery" },
    { id: "cst-004", name: "Etic" }
]
const employeeList = [
    { id: "emp-001", name: "Mr. Apisit Sukjai" },
    { id: "emp-002", name: "Mr. Somchai Sukjai" },
    { id: "emp-003", name: "Mrs. orione Sukjai" }
]


export default function Page() {

    const [projectInfo, setprojectInfo] = useState({
        project_name: "",
        project_leader: "",
        project_approver: "",
        plan_start_date: "",
        plan_end_date: "",
        actual_start_date: "",
        actual_end_date: "",
        md_policy: "",
        what: "",
        when: "",
        where: "",
        how: "",
        why: "",
        impact: "",
        scope: "",
        est_investment: 0,
        actual_investment: 0,

    })
    const [costSavingList, setcostSavingList] = useState([
        {
            cost_saving_type: "",
            cost_saving_amount: 0
        },
        {
            cost_saving_type: "",
            cost_saving_amount: 0
        },
        {
            cost_saving_type: "",
            cost_saving_amount: 0
        }
    ])
    const [memberList, setmemberList] = useState([
        { id: "", name: "" },
        { id: "", name: "" }
    ])
    const [activityList, setactivityList] = useState([])
    const [tabActive, settabActive] = useState(0)
    const [divisionList, setdivisionList] = useState([
        { id: "div01", name: "Sales" },
        { id: "div02", name: "Marketing" },
        { id: "div03", name: "Support" }
    ])
    const [divisionFilter, setdivisionFilter] = useState([])
    const [project_name, setproject_name] = useState("")
    function onchangeProjectName(value: string) {
        setproject_name(value)
        setdivisionFilter(divisionList.filter((div) => (div.name.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) > -1)))
    }

    function onclickSelectProjectName(value: string) {
        setproject_name(value)
        setdivisionFilter([])
    }

    function onclickInputProjectName() {
        if (project_name === "") {
            setdivisionFilter(divisionList)
        } else {
            setdivisionFilter(divisionList.filter((div) => (div.name.toLocaleLowerCase().indexOf(project_name.toLocaleLowerCase()) > -1)))
        }
    }

    function onBlurInputProjectName() {
        const divSelect = divisionList.filter((div) => (div.name.toLocaleLowerCase() === project_name.toLocaleLowerCase()))
        if (divSelect.length !== 0) {
            setproject_name(divSelect[0].name)
        } else {
            setproject_name("")
        }
        setTimeout(() => { setdivisionFilter([]) }, 100)
    }


    function onchangeCostSaving(key: string, row: number, value: string | number) {
        setcostSavingList(prev => prev.map((cst, index) => {
            if (index == row) {
                return { ...cst, [key]: value }
            } else {
                return cst
            }
        }))
    }
    useEffect(() => {
        console.log("costSavingList", costSavingList);

    }, [costSavingList])

    useEffect(() => {
        console.log("memberList", memberList)
    }, [memberList])

    const [employeeFilter, setemployeeFilter] = useState([

    ])

    function onchangeInputMember(key: string, row: number, value: string) {
        setmemberList(prev => prev.map((cst, index) => {
            if (index == row) {
                return { ...cst, [key]: value }
            } else {
                return cst
            }
        }))
        setemployeeFilter(employeeList.filter((emp) => (emp.name.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) > -1)))
    }

    const [employeeFilterActive, setemployeeFilterActive] = useState(0)
    function onclickInputMember(key: string, row: number, value: string) {
        setemployeeFilterActive(row)
        if (value === "") {
            setemployeeFilter(employeeList)
        } else {
            setemployeeFilter(employeeList.filter((emp) => (emp.name.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) > -1)))
        }
    }

    function onClickSelectMember(row: number, id: string, name: string) {
        setmemberList(prev => prev.map((mem, index) => {
            if (index == row) {
                return { ...mem, id, name }
            } else {
                return mem
            }
        }))
        setemployeeFilterActive(0)
    }

    function onBlurInputMember(row: number, value: string) {
        const empSelect = employeeList.filter((emp) => (emp.name.toLocaleLowerCase() === value.toLocaleLowerCase()))
        if (empSelect.length !== 0) {
            setmemberList(prev => prev.map((mem, index) => {
                if (index == row) {
                    return { ...mem, id: empSelect[0].id, name: empSelect[0].name }
                } else {
                    return mem
                }
            }))
        } else {
            setmemberList(prev => prev.map((mem, index) => {
                if (index == row) {
                    return { ...mem, id: "", name: "" }
                } else {
                    return mem
                }
            }))
        }
        setTimeout(() => { setemployeeFilter([]) }, 100)
    }
    useEffect(() => {
        console.log("employeeFilter", employeeFilter)
    }, [employeeFilter])

    const [newActivity, setnewActivity] = useState({
        activity_id: "",
        activity_name: "",
        weight: 0,
        plan_start: "",
        plan_end: "",
        pics: [
            { id: "", name: "" }
        ]
    })
    return (
        <div className="ml-[256px]">
            <div className="p-[12px]">
                <ul className="flex">
                    <li onClick={() => { settabActive(0) }} className={`px-[12px] border-l-[1px] border-r-[1px] ${tabActive === 0 ? "border-t-[1px]" : "border-t-[1px] border-b-[1px]"}`}>Project Information</li>
                    <li onClick={() => { settabActive(1) }} className={`px-[12px] border-l-[1px] border-r-[1px] ml-[-1px] ${tabActive === 1 ? "border-t-[1px]" : "border-t-[1px] border-b-[1px]"}`}>Benefit / Investment</li>
                    <li onClick={() => { settabActive(2) }} className={`px-[12px] border-l-[1px] border-r-[1px] ml-[-1px] ${tabActive === 2 ? "border-t-[1px]" : "border-t-[1px] border-b-[1px]"}`}>Team Members</li>
                    <li onClick={() => { settabActive(3) }} className={`px-[12px] border-l-[1px] border-r-[1px] ml-[-1px] ${tabActive === 3 ? "border-t-[1px]" : "border-t-[1px] border-b-[1px]"}`}>Project Plan</li>
                    <li onClick={() => { settabActive(4) }} className={`px-[12px] border-l-[1px] border-r-[1px] ml-[-1px] ${tabActive === 4 ? "border-t-[1px]" : "border-t-[1px] border-b-[1px]"}`}>Tools and Techniques</li>
                </ul>
            </div>

            <div className={tabActive === 0 ? "block" : "hidden"}>
                <div className="border-b-[1px] mx-[12px] mb-[6px]">Project Detail</div>
                <div className="flex flex-wrap mx-[12px]">
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="project_name"><span className="text-red-500">*</span> Project Name :</label>
                        <input className="w-[100%] border rounded" autoComplete="off" id="project_name" type="text"
                            value={projectInfo.project_name} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block"><span className="text-red-500">*</span> Project Plan :</label>
                        <div className="flex">
                            <input className="w-[50%] border rounded mr-[3px]" autoComplete="off" type="text" />
                            <input className="w-[50%] border rounded ml-[3px]" autoComplete="off" type="text" />
                        </div>
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="md_policy"><span className="text-red-500">*</span> MD Policy :</label>
                        <select id="md_policy" value={projectInfo.md_policy} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }}>
                            <option className={`${projectInfo.md_policy === "" ? "block" : "hidden"}`} value={""}></option>
                            {
                                costSavingTypeList.map((cst) => (
                                    <option key={`cost_saving_type_${cst.id}`} value={cst.id}>{cst.name}</option>
                                ))
                            }
                        </select>
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="project_what"><span className="text-red-500">*</span> What :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="project_what" />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="when"><span className="text-red-500">*</span> When :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="when"
                            value={projectInfo.when} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="where"><span className="text-red-500">*</span> Where :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="where"
                            value={projectInfo.where} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="how"><span className="text-red-500">*</span> How :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="how"
                            value={projectInfo.how} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="why"><span className="text-red-500">*</span> Why :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="why"
                            value={projectInfo.why} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="impact"><span className="text-red-500">*</span> Impact :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="impact"
                            value={projectInfo.impact} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>
                    <div className="w-[100%] p-[6px]">
                        <label className="block" htmlFor="scope"><span className="text-red-500">*</span> Scope :</label>
                        <textarea className="w-[100%] border rounded resize-none" autoComplete="off" id="scope"
                            value={projectInfo.scope} onChange={(e) => { setprojectInfo(prev => prev = { ...prev, [e.target.id]: e.target.value }) }} />
                    </div>

                    {/* <div className="w-[100%] p-[6px] relative">
                        <label className="block" htmlFor="project_name"><span className="text-red-500">*</span> Project Name :</label>
                        <input className="w-[100%] border rounded" autoComplete="off" id="project_name" type="text"
                            onBlur={() => { onBlurInputProjectName() }}
                            onClick={(e) => {
                                onclickInputProjectName()
                            }}
                            value={project_name} onChange={(e) => { onchangeProjectName(e.target.value) }} />
                        <ul className="absolute">
                            {
                                divisionFilter.map((div, index) => (
                                    <li key={`div-${index}`} onClick={() => { onclickSelectProjectName(div.name) }}>{div.name}</li>
                                ))
                            }
                        </ul>
                    </div> */}

                </div>
            </div>
            <div className={tabActive === 1 ? "block" : "hidden"}>
                <div className="flex">
                    <div className="w-[40%]">Cost Saving Type</div>
                    <div className="w-[40%]">Cost Saving Amount</div>
                    <div className="w-[20%]"></div>
                </div>
                <div className="flex flex-wrap">
                    {
                        costSavingList.map((cs, index) => (
                            <div key={`cost_saving_list_${index}`} className="flex w-[100%]">
                                <div className="w-[40%]">
                                    <select value={cs.cost_saving_type} onChange={(e) => { onchangeCostSaving("cost_saving_type", index, e.target.value) }}>
                                        <option className={`${cs.cost_saving_type === "" ? "block" : "hidden"}`} value={""}></option>
                                        {
                                            costSavingTypeList.map((cst) => (
                                                <option key={`cost_saving_type_${cst.id}`} value={cst.id}>{cst.name}</option>
                                            ))
                                        }
                                    </select>
                                </div>
                                <div className="w-[40%]">
                                    <input className="w-[100%] border rounded" disabled={cs.cost_saving_type === ""} value={cs.cost_saving_amount} onChange={(e) => { onchangeCostSaving("cost_saving_amount", index, e.target.value) }} type="text" />
                                </div>
                                <div className="w-[20%]">X</div>
                            </div>
                        ))
                    }
                </div>
            </div>
            <div className={tabActive === 2 ? "block" : "hidden"}>
                <div className="flex flex-wrap mx-[12px]">
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="project_leader"><span className="text-red-500">*</span> Project Leader :</label>
                        <input className="w-[100%] border rounded" readOnly id="project_leader" type="text" />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label className="block" htmlFor="project_approver"><span className="text-red-500">*</span> Project Approver :</label>
                        <input className="w-[100%] border rounded" readOnly id="project_approver" type="text" />
                    </div>
                </div>
                <div className="flex">
                    <div className="w-[90%]">Member Name</div>
                    <div className="w-[10%]"></div>
                </div>
                <div className="flex flex-wrap">
                    {
                        memberList.map((mem, index) => (
                            <div className="flex">
                                <div className="w-[90%]">
                                    <input className="border" value={mem.name}
                                        onBlur={(e) => { onBlurInputMember(index, mem.name) }}
                                        onClick={(e) => { onclickInputMember("name", index + 1, mem.name) }}
                                        onChange={(e) => { onchangeInputMember("name", index, e.target.value) }}
                                        type="text" />
                                    <ul className={`absolute ${employeeFilterActive === index + 1 ? "block" : "hidden"}`}>
                                        {
                                            employeeFilter.map((emp) => (
                                                <li onClick={() => { onClickSelectMember(index, emp.id, emp.name) }}>{emp.name} ({emp.id})</li>
                                            ))
                                        }
                                    </ul>
                                </div>
                                <div className="w-[10%]"></div>
                            </div>
                        ))
                    }
                </div>
            </div>
            <div className={tabActive === 3 ? "block" : "hidden"}>

                <button> + Add Activity</button>

                <div className="flex">
                    <div className="w-[20%]">Activity</div>
                    <div className="w-[20%]">Plan Start</div>
                    <div className="w-[20%]">Plan End</div>
                    <div className="w-[20%]">PICs</div>
                    <div className="w-[20%]">Weight</div>
                </div>

                <div className="flex">
                    <div className="w-[20%]">Activity</div>
                    <div className="w-[20%]">Plan Start</div>
                    <div className="w-[20%]">Plan End</div>
                    <div className="w-[20%]">PICs</div>
                    <div className="w-[20%]">Weight</div>
                </div>
                <div className="flex flex-wrap mx-[12px]">
                    <div className="w-[50%] p-[6px]">
                        <label htmlFor="activity_name">Activity Name</label>
                        <input id="activity_name" type="text" />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label htmlFor="activity_name">Weight</label>
                        <input id="activity_name" type="text" />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label htmlFor="activity_name">Plan Start</label>
                        <input id="activity_name" type="text" />
                    </div>
                    <div className="w-[50%] p-[6px]">
                        <label htmlFor="activity_name">Plan End</label>
                        <input id="activity_name" type="text" />
                    </div>
                    <div className="w-[100%] p-[6px]">
                        <div className="flex">
                            <div className="w-[90%]">PICs</div>
                            <div className="w-[10%]"></div>
                        </div>
                        <div className="flex flex-wrap">
                            {
                                newActivity.pics.map((pic) => (
                                    <div className="flex w-[100%]">
                                        <div className="w-[90%]">
                                            <select>
                                                <option className={`${pic.id === "" ? "block" : "hidden"}`} value={pic.id}>{pic.name}</option>
                                                {
                                                    employeeList.map((emp) => (
                                                        <option value={emp.id}>{emp.name}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>
                                        <div className="w-[10%]">X</div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>
                <div className={tabActive === 4 ? "block" : "hidden"}></div>
            </div>
        </div>
    )
}